package com.example.demo.service;

import com.example.demo.entities.Subject;
import com.example.demo.payload.request.SubjectCreateRequest;
import com.example.demo.payload.request.SubjectUpdateRequest;
import com.example.demo.payload.response.SubjectResponse;
import com.example.demo.repositories.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SubjectService {
    private final SubjectRepository subjectRepository;

    @Autowired
    public SubjectService(SubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    public List<SubjectResponse> getAll(){
        return subjectRepository.findAll().stream()
                .map(SubjectResponse::fromSubject)
                .collect(Collectors.toList());
    }

    @Transactional
    public void update(SubjectUpdateRequest subjectUpdateRequest){
        subjectRepository.updateSubjectInfoById(
                subjectUpdateRequest.getSubjectId(),
                subjectUpdateRequest.getRequirements(),
                subjectUpdateRequest.getMaxCapacity()
        );
    }


    public SubjectResponse getSubject(long id){
        return subjectRepository.findById(id).map(SubjectResponse::fromSubject).orElseThrow(
                () -> new EntityNotFoundException(String.format("Subject with ID %s not found", id))
        );
    }

    @Transactional
    public void delete(long id){
        subjectRepository.deleteById(id);
    }


    public void save(SubjectCreateRequest subjectCreateRequest){
        Subject subject = subjectCreateRequest.asSubject();
        subjectRepository.save(subject);
    }
}
