package com.example.demo.service;
import com.example.demo.entities.Classroom;
import com.example.demo.payload.request.ClassroomCreateRequest;
import com.example.demo.payload.request.ClassroomUpdateRequest;
import com.example.demo.payload.response.ClassroomResponse;
import com.example.demo.repositories.ClassroomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClassroomService {
    private final ClassroomRepository classroomRepository;

    @Autowired
    public ClassroomService(ClassroomRepository classroomRepository) {
        this.classroomRepository = classroomRepository;
    }

    public List<ClassroomResponse> getAll(){
        return classroomRepository.findAll().stream()
                .map(ClassroomResponse::fromClassroom)
                .collect(Collectors.toList());
    }

    @Transactional
    public void update(ClassroomUpdateRequest classroomUpdateRequest){
        classroomRepository.updateClassroomInfoById(
                classroomUpdateRequest.getClassroomId(),
                classroomUpdateRequest.getCapacity(),
                classroomUpdateRequest.getFacilities(),
                classroomUpdateRequest.getClassId()

        );
    }

    public ClassroomResponse getClassroom(long id){
        return classroomRepository.findById(id).map(ClassroomResponse::fromClassroom).orElseThrow(
                () -> new EntityNotFoundException(String.format("Classroom with ID %s not found", id))
        );
    }

    @Transactional
    public void delete(long id){
        classroomRepository.deleteById(id);
    }


    public void save(ClassroomCreateRequest classroomCreateRequest){
        Classroom classroom = classroomCreateRequest.asClassroom();
        classroomRepository.save(classroom);
    }
}
