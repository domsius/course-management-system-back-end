package com.example.demo.service;

import com.example.demo.entities.Role;
import com.example.demo.enums.RoleAuthority;
import com.example.demo.payload.response.RoleResponse;
import com.example.demo.payload.response.UserResponse;
import com.example.demo.repositories.RoleRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleService {
    private final RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public Role findByRoleName(RoleAuthority name) throws NotFoundException {
        return roleRepository.findByNameEnum(name).orElseThrow(() -> new NotFoundException(String.format("Role name %s not found", name)));
    }

    public List<UserResponse> getUserListByRole(String roleNameAsString) {
        Role role = roleRepository.findByNameEnum(RoleAuthority.valueOf(roleNameAsString)).orElseThrow(
                () -> new EntityNotFoundException(String.format("Role %s not exists", roleNameAsString)));

        return role.getUsersList().stream()
                .map(UserResponse::fromUser).collect(Collectors.toList());
    }

    public List<RoleResponse> getAll(){
        return this.roleRepository.findAll().stream()
                .map(RoleResponse::fromRole).collect(Collectors.toList());
    }

    public Role findById(long id){
        return roleRepository.findById(id);
    }
}
