package com.example.demo.service;

import com.example.demo.entities.Classroom;
import com.example.demo.entities.StudentClasses;
import com.example.demo.payload.request.*;
import com.example.demo.payload.response.ClassroomResponse;
import com.example.demo.payload.response.StudentClassesResponse;
import com.example.demo.repositories.StudentClassesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentClassesService {
    private final StudentClassesRepository studentClassesRepository;

    @Autowired
    public StudentClassesService(StudentClassesRepository studentClassesRepository) {
        this.studentClassesRepository = studentClassesRepository;
    }

    public List<StudentClassesResponse> getAll(){
        return studentClassesRepository.findAll().stream()
                .map(StudentClassesResponse::fromStudentClasses)
                .collect(Collectors.toList());
    }

    @Transactional
    public void update(StudentClassesUpdateRequest studentClassesUpdateRequest){
        studentClassesRepository.updateStudentClassesInfoById(
                studentClassesUpdateRequest.getId(),
                studentClassesUpdateRequest.getClassId(),
                studentClassesUpdateRequest.getStudentId()
        );
    }

    public StudentClassesResponse getClassroom(long id){
        return studentClassesRepository.findById(id).map(StudentClassesResponse::fromStudentClasses).orElseThrow(
                () -> new EntityNotFoundException(String.format("Classroom with ID %s not found", id))
        );
    }

    @Transactional
    public void delete(long id){
        studentClassesRepository.deleteById(id);
    }


    public void save(StudentClassesCreateRequest studentClassesCreateRequest){
        StudentClasses studentClasses = studentClassesCreateRequest.asStudentClasses();
        studentClassesRepository.save(studentClasses);
    }
}
