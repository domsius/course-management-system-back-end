package com.example.demo.service;

import com.example.demo.entities.Course;
import com.example.demo.payload.request.CourseCreateRequest;
import com.example.demo.payload.request.CourseUpdateRequest;
import com.example.demo.payload.response.CourseResponse;
import com.example.demo.payload.response.UserResponse;
import com.example.demo.repositories.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CourseService {
    private final CourseRepository courseRepository;

    @Autowired
    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public List<CourseResponse> getAll(){
        return courseRepository.findAll().stream()
                .map(CourseResponse::fromCourse)
                .collect(Collectors.toList());
    }

    @Transactional
    public void update(CourseUpdateRequest courseUpdateRequest){
        courseRepository.updateCourseInfoById(
                courseUpdateRequest.getClassroom(),
                courseUpdateRequest.getSubject(),
                courseUpdateRequest.getPeriod(),
                courseUpdateRequest.getTime(),
                courseUpdateRequest.getCourseId()

                );
    }

    public CourseResponse getCourse(long id){
        return courseRepository.findById(id).map(CourseResponse::fromCourse).orElseThrow(
                () -> new EntityNotFoundException(String.format("User with ID %s not found", id))
        );
    }

    @Transactional
    public void delete(long id){
        courseRepository.deleteById(id);
    }

    public void save(CourseCreateRequest courseCreateRequest){

        Course course = courseCreateRequest.asCourse(courseCreateRequest.getClassroom(), courseCreateRequest.getSubject());
        courseRepository.save(course);
    }
}
