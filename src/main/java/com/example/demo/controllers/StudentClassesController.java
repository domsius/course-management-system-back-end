package com.example.demo.controllers;

import com.example.demo.payload.response.StudentClassesResponse;
import com.example.demo.service.StudentClassesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

public class StudentClassesController {

    private final StudentClassesService studentClassesService;


    @Autowired
    public StudentClassesController(StudentClassesService studentClassesService) {
        this.studentClassesService = studentClassesService;
    }


    @GetMapping("/student-classes")
    public List<StudentClassesResponse> getAll() {
        System.out.println("Getting all student-classes....");
        return studentClassesService.getAll();
    }
}
