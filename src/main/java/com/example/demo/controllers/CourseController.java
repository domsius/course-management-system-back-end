package com.example.demo.controllers;


import com.example.demo.payload.request.CourseCreateRequest;
import com.example.demo.payload.request.CourseUpdateRequest;
import com.example.demo.payload.response.CourseResponse;
import com.example.demo.payload.response.UserResponse;
import com.example.demo.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/v1")
public class CourseController {

    private final CourseService courseService;


    @Autowired
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }


    @GetMapping("/course")
    public List<CourseResponse> getAll(){
        System.out.println("Getting all courses....");
        return courseService.getAll();
    }
    @GetMapping("/course/{id}")
    public CourseResponse getCourse(@PathVariable("id") long id){
        return courseService.getCourse(id);
    }

    @PutMapping("/course/{id}")
    public void update(@RequestBody @Valid CourseUpdateRequest courseUpdateRequest, @PathVariable String id){
        courseService.update(courseUpdateRequest);
    }

    @PostMapping("/course/add")
    public void createCourse(@RequestBody @Valid CourseCreateRequest courseCreateRequest){
        courseService.save(courseCreateRequest);
    }

    @DeleteMapping("/course/{id}")
    public void delete(@PathVariable("id") long id){
        courseService.delete(id);
    }
}
