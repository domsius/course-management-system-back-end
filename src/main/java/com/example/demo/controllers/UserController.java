package com.example.demo.controllers;

import com.example.demo.entities.User;
import com.example.demo.payload.request.UserCreateRequest;
import com.example.demo.payload.request.UserUpdateRequest;
import com.example.demo.payload.response.UserResponse;

import com.example.demo.service.UserService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
;import java.util.List;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/v1")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/user")
    public List<UserResponse> getAll(){
        System.out.println("Getting users....");
        return userService.getAll();
    }

    @GetMapping("/user/{id}")
    public UserResponse getUser(@PathVariable("id") long id){
        return userService.getUser(id);
    }

    @PostMapping("/user/add")
    public void register(@RequestBody @Valid UserCreateRequest userCreateRequest) throws NotFoundException {
        System.out.println("User registration....");
        userService.save(userCreateRequest);
    }
    @PutMapping("/user/{id}")
    public void update(@RequestBody @Valid UserUpdateRequest userUpdateRequest, @PathVariable String id) throws NotFoundException {
        System.out.println(userUpdateRequest);
        userService.update(userUpdateRequest);
    }

    @DeleteMapping("/user/{id}")
    public void delete(@PathVariable("id") long id){
        userService.delete(id);
    }
}
