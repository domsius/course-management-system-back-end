package com.example.demo.controllers;

import com.example.demo.payload.request.ClassroomCreateRequest;
import com.example.demo.payload.request.ClassroomUpdateRequest;
import com.example.demo.payload.request.CourseCreateRequest;
import com.example.demo.payload.request.CourseUpdateRequest;
import com.example.demo.payload.response.ClassroomResponse;
import com.example.demo.payload.response.CourseResponse;
import com.example.demo.service.ClassroomService;
import com.example.demo.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ClassroomController {

    private final ClassroomService classroomService;


    @Autowired
    public ClassroomController(ClassroomService classroomService) {
        this.classroomService = classroomService;
    }


    @GetMapping("/classroom")
    public List<ClassroomResponse> getAll(){
        System.out.println("Getting all classrooms....");
        return classroomService.getAll();
    }
    @GetMapping("/classroom/{id}")
    public ClassroomResponse getClassroom(@PathVariable("id") long id){
        return classroomService.getClassroom(id);
    }

    @PutMapping("/classroom/{id}")
    public void update(@RequestBody @Valid ClassroomUpdateRequest classroomUpdateRequest, @PathVariable String id){
        classroomService.update(classroomUpdateRequest);
    }

    @PostMapping("/classroom/add")
    public void createCourse(@RequestBody @Valid ClassroomCreateRequest classroomCreateRequest){
        classroomService.save(classroomCreateRequest);
    }

    @DeleteMapping("/classroom/{id}")
    public void delete(@PathVariable("id") long id){
        classroomService.delete(id);
    }
}
