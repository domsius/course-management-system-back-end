package com.example.demo.payload.response;

import com.example.demo.entities.Classroom;
import com.example.demo.entities.StudentClasses;
import com.example.demo.entities.Subject;

public class StudentClassesResponse {

    private long id;

            public StudentClassesResponse(long id) {
            this.id = id;

        }

    public static StudentClassesResponse fromStudentClasses(StudentClasses studentClasses){
        return new StudentClassesResponse(studentClasses.getId());
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


}
