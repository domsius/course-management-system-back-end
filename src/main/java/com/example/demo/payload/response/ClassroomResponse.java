package com.example.demo.payload.response;

import com.example.demo.entities.Classroom;
public class ClassroomResponse {


    private final long classroomId;
    private final int capacity;
    private final String facilities;
    private final long courseId;

    public ClassroomResponse(long classroomId, int capacity, String facilities, long courseId) {
        this.classroomId = classroomId;
        this.capacity = capacity;
        this.facilities = facilities;
        this.courseId = courseId;
    }

    public static ClassroomResponse fromClassroom(Classroom classroom){
        return new ClassroomResponse(classroom.getClassroomId(), classroom.getCapacity(), classroom.getFacilities(), classroom.getCourseId());
    }

    public long getClassroomId() {
        return classroomId;
    }

    public int getCapacity() {
        return capacity;
    }

    public String getFacilities() {
        return facilities;
    }

    public long getCourseId() {
        return courseId;
    }
}
