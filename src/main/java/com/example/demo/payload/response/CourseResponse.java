package com.example.demo.payload.response;
import com.example.demo.entities.Course;
import java.time.LocalDateTime;

public class CourseResponse {

    private final long courseId;
    private final ClassroomResponse classroom;
    private final SubjectResponse subject;
    private final String period;
    private final LocalDateTime time;


    public CourseResponse(long courseId, ClassroomResponse classroom, SubjectResponse subject, String period, LocalDateTime time) {
        this.courseId = courseId;
        this.classroom = classroom;
        this.subject = subject;
        this.period = period;
        this.time = time;


    }

    public static CourseResponse fromCourse(Course course){
        ClassroomResponse classroomResponse = ClassroomResponse.fromClassroom(course.getClassroom());
        SubjectResponse subjectResponse = SubjectResponse.fromSubject(course.getSubject());
        return new CourseResponse(course.getCourseId(), classroomResponse, subjectResponse, course.getPeriod(), course.getTime());
    }

    public long getCourseId() {
        return courseId;
    }

    public ClassroomResponse getClassroom() {
        return classroom;
    }

    public SubjectResponse getSubject() {
        return subject;
    }

    public String getPeriod() {
        return period;
    }

    public LocalDateTime getTime() {
        return time;
    }
}
