package com.example.demo.payload.response;
import com.example.demo.entities.Subject;

public class SubjectResponse {


    private final long subjectId;
    private final int requirements;
    private final int maxCapacity;

    public SubjectResponse(long subjectId, int requirements, int maxCapacity) {
        this.subjectId = subjectId;
        this.requirements = requirements;
        this.maxCapacity = maxCapacity;


    }

    public static SubjectResponse fromSubject(Subject subject){
        return new SubjectResponse(subject.getSubjectId(), subject.getRequirements(), subject.getMaxCapacity());
    }

    public long getSubjectId() {
        return subjectId;
    }

    public int getRequirements() {
        return requirements;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }
}
