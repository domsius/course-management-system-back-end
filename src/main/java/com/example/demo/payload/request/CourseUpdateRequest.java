package com.example.demo.payload.request;

import com.example.demo.entities.Course;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

public class CourseUpdateRequest {

    private final Course courseId;
    private final long classroom;
    private final long subject;
    private final String period;
    private final LocalDateTime time;

    @JsonCreator
    public CourseUpdateRequest(
            @JsonProperty("courseId") final Course courseId,
            @JsonProperty("classroomId") final long classroom,
            @JsonProperty("subjectId") final long subject,
            @JsonProperty("period") final String period,
            @JsonProperty("time") final LocalDateTime time){
        this.courseId = courseId;
        this.classroom = classroom;
        this.subject = subject;
        this.period = period;
        this.time = time;
    }

    public long getClassroom() {
        return classroom;
    }

    public long getSubject() {
        return subject;
    }

    public String getPeriod() {
        return period;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public Course getCourseId() {
        return courseId;
    }


}
