package com.example.demo.payload.request;

import com.example.demo.entities.Classroom;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;

public class ClassroomCreateRequest {
    @NotBlank(message = "Classroom ID must be set")
    private long classroomId;

    @NotBlank(message = "Capacity must be set")
    private int capacity;

    @NotBlank(message = "Facilities must be set")
    private String facilities;

    @NotBlank(message = "Class ID must be set")
    private long classId;


    @JsonCreator
    public ClassroomCreateRequest(
            @JsonProperty("classroomId") final long classroomId,
            @JsonProperty("capacity") final int capacity,
            @JsonProperty("facilities") final String facilities,
            @JsonProperty("classId") final long classId) {
        this.classroomId = classroomId;
        this.capacity = capacity;
        this.facilities = facilities;
        this.classId = classId;
    }

    public Classroom asClassroom(){
        return new Classroom(classroomId, capacity, facilities, classId);
    }

    public long getClassroomId() {
        return classroomId;
    }

    public void setClassroomId(long classroomId) {
        this.classroomId = classroomId;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    public long getClassId() {
        return classId;
    }

    public void setClassId(long classId) {
        this.classId = classId;
    }
}
