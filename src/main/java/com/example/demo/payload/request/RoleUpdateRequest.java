package com.example.demo.payload.request;

import com.example.demo.enums.RoleAuthority;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RoleUpdateRequest {
    private final long id;
    private final RoleAuthority nameEnum;
    private final String name;

    @JsonCreator
    public RoleUpdateRequest(
            @JsonProperty("id") long id,
            @JsonProperty("nameEnum") String nameEnum,
            @JsonProperty("name") String name) {
        this.id = id;
        this.nameEnum = RoleAuthority.valueOf(nameEnum);
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public RoleAuthority getNameEnum() {
        return nameEnum;
    }

    public String getName() {
        return name;
    }
}
