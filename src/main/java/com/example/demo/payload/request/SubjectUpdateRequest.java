package com.example.demo.payload.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SubjectUpdateRequest {

    private final long subjectId;
    private final int requirements;
    private final int maxCapacity;


    @JsonCreator
    public SubjectUpdateRequest(
            @JsonProperty("subjectId") final long subjectId,
            @JsonProperty("requirements") final int requirements,
            @JsonProperty("maxCapacity") final int maxCapacity){
        this.subjectId = subjectId;
        this.requirements = requirements;
        this.maxCapacity = maxCapacity;


    }

    public long getSubjectId() {
        return subjectId;
    }

    public int getRequirements() {
        return requirements;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }
}
