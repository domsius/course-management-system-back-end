package com.example.demo.payload.request;

import com.example.demo.entities.StudentClasses;
import com.example.demo.entities.Subject;
import com.example.demo.entities.User;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;

public class StudentClassesCreateRequest {
    @NotBlank(message = "ID must be set")
    private final long id;

    @NotBlank(message = "Student ID must be set")
    private final User user;

    @JsonCreator
    public StudentClassesCreateRequest(
            @JsonProperty("id") final long id,
            @JsonProperty("studentId") final User user) {
        this.id = id;
        this.user = user;
    }

    public StudentClasses asStudentClasses() {
        return new StudentClasses(id, user);
    }

}
