package com.example.demo.payload.request;

import com.example.demo.entities.Role;
import com.example.demo.entities.User;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotBlank;

public class UserCreateRequest {
    @NotBlank(message = "Username must be set")
    private final String username;

    @NotBlank(message = "Password must be set")
    private final String password;

    @NotBlank(message = "First name must be set")
    private final String firstName;

    @NotBlank(message = "Last name must be set")
    private final String lastName;


    @JsonCreator
    public UserCreateRequest(
            @JsonProperty("username") final String username,
            @JsonProperty("password") final String password,
            @JsonProperty("firstName") final String firstName,
            @JsonProperty("lastName") final String lastName

    ) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;


    }

    public User asUser(Role role) {
        return new User(username, password, firstName, lastName, role);
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
