package com.example.demo.payload.request;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class ClassroomUpdateRequest {

    private final long classroomId;
    private final int capacity;
    private final String facilities;
    private final long classId;


    @JsonCreator
    public ClassroomUpdateRequest(
            @JsonProperty("classroomId") final long classroomId,
            @JsonProperty("capacity") final int capacity,
            @JsonProperty("facilities") final String facilities,
            @JsonProperty("classId") final long classId){
        this.classroomId = classroomId;
        this.capacity = capacity;
        this.facilities = facilities;
        this.classId = classId;

    }

    public long getClassroomId() {
        return classroomId;
    }

    public int getCapacity() {
        return capacity;
    }

    public String getFacilities() {
        return facilities;
    }

    public long getClassId() {
        return classId;
    }
}
