package com.example.demo.payload.request;

import com.example.demo.entities.Classroom;
import com.example.demo.entities.Subject;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;

public class SubjectCreateRequest {
    @NotBlank(message = "Subject ID must be set")
    private final long subjectId;

    @NotBlank(message = "Requirements must be set")
    private final int requirements;

    @NotBlank(message = "Max Capacity must be set")
    private final int maxCapacity;

    @JsonCreator
    public SubjectCreateRequest(
            @JsonProperty("subjectId") final long subjectId,
            @JsonProperty("requirements") final int requirements,
            @JsonProperty("maxCapacity") final int maxCapacity) {
        this.subjectId = subjectId;
        this.requirements = requirements;
        this.maxCapacity = maxCapacity;
    }

    public Subject asSubject(){
        return new Subject(subjectId, requirements, maxCapacity);
    }

    public long getSubjectId() {
        return subjectId;
    }

    public int getRequirements() {
        return requirements;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }
}
