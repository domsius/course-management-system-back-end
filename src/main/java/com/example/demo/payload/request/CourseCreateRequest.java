package com.example.demo.payload.request;


import com.example.demo.entities.*;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

public class CourseCreateRequest {
    @NotBlank(message = "Classroom must be set")
    private Classroom classroom;

    @NotBlank(message = "Subject ID must be set")
    private Subject subject;

    @NotBlank(message = "Period must be set")
    private String period;

    @NotBlank(message = "Time must be set")
    private LocalDateTime time;



    @JsonCreator
    public CourseCreateRequest(
            @JsonProperty("classroom") final Classroom classroom,
            @JsonProperty("subject") final Subject subject,
            @JsonProperty("period") final String period,
            @JsonProperty("time") final LocalDateTime time){
        this.classroom = classroom;
        this.subject = subject;
        this.period = period;
        this.time = time;
    }

    public Course asCourse(Classroom classroom,
                           Subject subject){

        return new Course(classroom, subject, period, time);
    }

    public Classroom getClassroom() {
        return classroom;
    }

    public void setClassroom(Classroom classroom) {
        this.classroom = classroom;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }


}

