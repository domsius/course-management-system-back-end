package com.example.demo.payload.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class StudentClassesUpdateRequest {

    private final long id;
    private final long classId;
    private final long studentId;


    @JsonCreator
    public StudentClassesUpdateRequest(
            @JsonProperty("id") final long id,
            @JsonProperty("classId") final long classId,
            @JsonProperty("studentId") final long studentId){
        this.id = id;
        this.classId = classId;
        this.studentId = studentId;


    }

    public long getId() {
        return id;
    }

    public long getClassId() {
        return classId;
    }

    public long getStudentId() {
        return studentId;
    }
}
