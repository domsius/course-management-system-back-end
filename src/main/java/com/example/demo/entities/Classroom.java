package com.example.demo.entities;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
public class Classroom {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long classroomId;

    @NotBlank(message = "Capacity must be set")
    private int capacity;

    @NotBlank(message = "Facilities must be set")
    private String facilities;

    @NotBlank(message = "Class ID must be set")
    private long courseId;

    @OneToMany(mappedBy = "classroom", cascade = CascadeType.ALL)
    private List<Course> courseList;


    public Classroom(long classroomId, int capacity, String facilities,long courseId) {
        this.classroomId = classroomId;
        this.capacity = capacity;
        this.facilities = facilities;
        this.courseId = courseId;
    }

    public Classroom() {

    }

    public long getClassroomId() {
        return classroomId;
    }

    public void setClassroomId(long classroomId) {
        this.classroomId = classroomId;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public List<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
    }
}
