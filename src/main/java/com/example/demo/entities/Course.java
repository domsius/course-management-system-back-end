package com.example.demo.entities;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long courseId;

    @ManyToOne
    @JoinColumn(name = "classroom_id")
    Classroom classroom;

    @ManyToOne
    @JoinColumn(name = "subject_id")
    Subject subject;

    @NotBlank(message = "Period must be set")
    private String period;

    @NotBlank(message = "Time must be set")
    private LocalDateTime time;


    public Course() {

    }


    public Course( Classroom classroom, Subject subject, String period,LocalDateTime time) {
        this.classroom = classroom;
        this.subject = subject;
        this.period = period;
        this.time = time;
    }

    public Classroom getClassroom() {
        return classroom;
    }

    public void setClassroom(Classroom classroom) {
        this.classroom = classroom;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }


}
