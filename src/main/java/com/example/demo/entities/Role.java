package com.example.demo.entities;

import com.example.demo.enums.RoleAuthority;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "roles")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
    private RoleAuthority nameEnum;

    private String name;

    @OneToMany(mappedBy = "userRole", cascade = CascadeType.ALL)
    private List<User> usersList;

    public Role() { }

    public long getId() {
        return id;
    }

    public RoleAuthority getNameEnum() {
        return nameEnum;
    }

    public void setNameEnum(RoleAuthority nameEnum) {
        this.nameEnum = nameEnum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<User> usersList) {
        this.usersList = usersList;
    }
}
