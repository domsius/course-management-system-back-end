package com.example.demo.entities;

import javax.persistence.*;

@Entity
public class StudentClasses {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "course_id")
    Course course;

    @ManyToOne
    @JoinColumn(name = "user_id")
    User user;

    public StudentClasses(long id, User user) {
        this.id = id;
        this.user = user;
    }

    public StudentClasses() {

    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
