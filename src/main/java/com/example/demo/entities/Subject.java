package com.example.demo.entities;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long subjectId;

    @NotBlank(message = "Requirements must be set")
    private int requirements;

    @NotBlank(message = "Max Capacity must be set")
    private int maxCapacity;

    @OneToMany(mappedBy = "subject", cascade = CascadeType.ALL)
    private List<Course> courseList;


    public Subject(long subjectId, int requirements, int maxCapacity) {
        this.subjectId = subjectId;
        this.requirements = requirements;
        this.maxCapacity = maxCapacity;
    }

    public Subject() {

    }

    public long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(long subjectId) {
        this.subjectId = subjectId;
    }

    public int getRequirements() {
        return requirements;
    }

    public void setRequirements(int requirements) {
        this.requirements = requirements;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }
}
