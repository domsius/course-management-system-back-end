package com.example.demo.repositories;



import com.example.demo.entities.Subject;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubjectRepository extends CrudRepository<Subject, Long> {
    List<Subject> findAll();

    @Modifying
    @Query("UPDATE Subject SET requirements = ?1, maxCapacity = ?2 WHERE subjectId = ?1")
    void updateSubjectInfoById(long subjectId, int requirements, int maxCapacity);
}

