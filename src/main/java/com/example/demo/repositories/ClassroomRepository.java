package com.example.demo.repositories;

import com.example.demo.entities.Classroom;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClassroomRepository extends CrudRepository<Classroom, Long> {
    List<Classroom> findAll();

    @Modifying
    @Query("UPDATE Classroom SET classroomId = ?1, capacity = ?2, facilities = ?3 WHERE classId = ?4")
    void updateClassroomInfoById(long classroomId, int capacity, String facilities, long classId);
}
