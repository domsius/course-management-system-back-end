package com.example.demo.repositories;

import com.example.demo.entities.Role;
import com.example.demo.enums.RoleAuthority;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface RoleRepository extends CrudRepository<Role, Long> {
    List<Role> findAll();

    Optional<Role> findByNameEnum(RoleAuthority name);
    Role findById(long id);
}
