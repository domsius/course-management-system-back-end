package com.example.demo.repositories;

import com.example.demo.entities.Classroom;
import com.example.demo.entities.Course;
import com.example.demo.entities.Subject;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface CourseRepository extends CrudRepository<Course, Long> {
    List<Course> findAll();

    @Modifying
    @Query("UPDATE Course SET classroom.classroomId = ?1, subject.subjectId = ?2, period = ?3, time = ?4 WHERE courseId = ?5")
    void updateCourseInfoById(long classroom, long subject, String period, LocalDateTime time, Course courseId);
}

