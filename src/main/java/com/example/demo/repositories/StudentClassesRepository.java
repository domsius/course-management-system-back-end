package com.example.demo.repositories;

import com.example.demo.entities.StudentClasses;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentClassesRepository extends CrudRepository<StudentClasses, Long> {
    List<StudentClasses> findAll();

    @Modifying
    @Query("UPDATE StudentClasses SET classId = ?2, studentId = ?3 WHERE id = ?1")
    void updateStudentClassesInfoById(long id, long classId, long studentId);
}

