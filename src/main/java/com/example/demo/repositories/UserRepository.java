package com.example.demo.repositories;

import com.example.demo.entities.Role;
import com.example.demo.entities.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;


@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    List<User> findAll();

    @Modifying
    @Query("UPDATE User SET firstName = ?2, lastName = ?3, userRole = ?4 WHERE id = ?1")
    void updateUserInfoById(long id, String firstName, String lastName, Role role);

    @Query("SELECT u.userRole FROM User u WHERE u.id = ?1")
    Role getUserRole(long id);

    Optional<User> findByUsername(String username);
    boolean existsByUsername(String username);
}
