package com.example.demo.enums;

public enum RoleAuthority {
    ROLE_STUDENT,
    ROLE_TEACHER,
    ROLE_ADMIN;
}
